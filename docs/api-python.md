<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# typed-format-version: Python API Reference

::: typed_format_version
    handler: python

<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# Changelog

All notable changes to the typed-format-version project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.1] - 2022-10-10

## Other changes

- Rust implementation:
    - use the thiserror library instead of the quick-error one
    - use the anyhow library to chain errors

## [0.2.0] - 2022-10-02

## Incompatible changes

- the Rust implementation's `get_format_from_value()` and
  `get_version_from_value()` functions dropped the "conversion function"
  argument, relying on the "value" argument to implement the `Deserializer`
  trait for its own contents

## Fixes

- Rust implementation:
    - turn `serde_json` and `serde_yaml` into `dev-dependencies`

## Additions

- Python implementation:
    - declare Python 3.11 as a supported version
    - add a Nix expression for running the Tox tests with different Python
      versions using `nix-shell`
    - add more files to the sdist tarball
- Rust implementation:
    - add `toml` to `dev-dependencies` and run the tests for TOML values, too
    - implement `Eq` for `Version`

## Other changes

- Global changes:
    - convert the test data files from JSON to TOML
- Python implementation:
    - drop the flake8 + hacking Tox test environment
    - add both lower and upper version constraints for the dependencies in
      the Tox test environments
    - drop `types-dataclasses` from the mypy Tox test environment
- Rust implementation:
    - minor refactoring for some Clippy lints
    - silence the `clippy::std_instead_of_core` lint in the `run-clippy` tool 
    - drop some silenced lints from `run-clippy` since we do not violate them
    - use the `tracing` and `tracing-test` crates for the test suite

## [0.1.0] - 2022-07-21

## Started

- First public release

[Unreleased]: https://gitlab.com/ppentchev/typed-format-version/-/compare/release%2F0.2.1...main
[0.2.1]: https://gitlab.com/ppentchev/typed-format-version/-/compare/release%2F0.2.0...release%2F0.2.1
[0.2.0]: https://gitlab.com/ppentchev/typed-format-version/-/compare/release%2F0.1.0...release%2F0.2.0
[0.1.0]: https://gitlab.com/ppentchev/typed-format-version/-/tags/release%2F0.1.0
